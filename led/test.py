from rgbmatrix import RGBMatrix
from rgbmatrix import graphics

matrix = RGBMatrix(32, 2)
canvas = matrix.CreateFrameCanvas()

print [method for method in dir(graphics) if callable(getattr(graphics, method))]
print [method for method in dir(canvas) if callable(getattr(canvas, method))]