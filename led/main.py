# -*- coding: utf-8 -*-

# encoding=utf8  
import sys  
import  RPi.GPIO as GPIO
import time
from datetime import datetime

reload(sys)  
sys.setdefaultencoding('utf8')

#skarmarbrink = 9188
#hammarbyhojden = 9144
#centralen = 1002

from leddisplay import LedDisplay

led = LedDisplay(32, 2)
led.addTrip("Hammarbyhöjden", "9144", "1002", 5)
led.addTrip("Skärmarbrink", "9188", "1002", 10)

GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.IN,pull_up_down=GPIO.PUD_UP)
lastReloaded = datetime(2005,01,01)

while True:
    inputValue = GPIO.input(21)
    if (inputValue == False):
        delta = datetime.now() - lastReloaded
    
        if(delta.seconds > 180):
            led.loadTrips()
            
        led.display()
    time.sleep(0.3)

