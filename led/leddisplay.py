import time
import sys
import Image
import ImageDraw
from datetime import datetime

sys.path.append("../sl")
import planner

from rgbmatrix import RGBMatrix
from rgbmatrix import graphics

class TripController:
    def __init__(self, name, fromId, toId, walkTime):
        self.name = name
        self.fromId = fromId
        self.toId = toId
        self.walkTime = walkTime
        self.alternatives = []
    
    def getColor(self, level):
        if level == 0:
            return graphics.Color(167,34,255)
        
        if level == 1:
            return graphics.Color(255,34,34)
        
        if level == 2:
            return graphics.Color(255,145,34)
            
        if level == 3:
            return graphics.Color(49,255,34)
            
        if level == 4:
            return graphics.Color(34,56,255)

        return graphics.Color(135,135,135)
        
    def load(self):
        trip = planner.Trip(self.fromId, self.toId, self.walkTime)
        self.alternatives = trip.getSuggestions()
        
    def initDisplay(self, row, font, width):
        self.row = row
        self.font = font
        self.width = width
        self.position = 2
        self.startFrom = 0
        
    def displayTitle(self, canvas):
        color = graphics.Color(32,255,5)
        font = graphics.Font()
        font.LoadFont('fonts/4x6.bdf')
        graphics.DrawText(canvas, font, self.position, self.row - 1, color, self.name)
        
    def display(self, canvas):
        self.displayTitle(canvas)
        insertFrom = self.position
        reset = True
        
        for index, alternative in enumerate(self.alternatives):
            if index < self.startFrom:
                continue    
            
            if insertFrom > self.width - 10:
                self.startFrom = index
                reset = False
                break
            
            color = self.getColor(alternative.getLevel())
            text = alternative.getDisplayValue()
            textWidth = graphics.DrawText(canvas, self.font, insertFrom, self.row + 7, color, text)
            insertFrom += textWidth + 3

        if reset:
            self.startFrom = 0
        
class LedDisplay:
    def __init__(self, rows, chains):
        self.trips = []
        self.matrix = RGBMatrix(rows, chains)
    
    def addTrip(self, name, fromId, toId, walkDistance):
        trip = TripController(name, fromId, toId, walkDistance)
        self.trips.append(trip)
        
    def loadTrips(self):
        for trip in self.trips:
            trip.load()
            
    def getFont(self):
        font = graphics.Font()
        font.LoadFont('fonts/6x9.bdf')
        return font
        
    def setBorder(self, canvas):
        for i in range(0, canvas.width):
            canvas.SetPixel(i, 0, 102,167,242)
            canvas.SetPixel(i, 31, 102,167,242)
            
            if(i < canvas.height):
                canvas.SetPixel(0, i, 102,167,242)
                canvas.SetPixel(63, i, 102,167,242)

    def display(self):
        canvas = self.matrix.CreateFrameCanvas()
        font = self.getFont()
        start = datetime.now()
        row = 8
        
        for trip in self.trips:
            trip.initDisplay(row, font, canvas.width)
            row += 14
        
        while True:
            canvas.Clear()
            self.setBorder(canvas)
            
            for trip in self.trips:
                trip.display(canvas)
            
            self.matrix.SwapOnVSync(canvas)
            time.sleep(5)
            
            delta = datetime.now() - start
            
            if delta.seconds > 30:
                break
        
        canvas.Clear()