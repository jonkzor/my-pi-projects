#http://api.sl.se/api2/TravelplannerV2/trip.json?key=<KEY>&originId=<FROM>&destId=<TO>&searchForArrival=0&realtime=true

import json
import urllib2
import datetime
import math

def addMins(date, mins):
    secs = mins*60
    print secs
    fulldate = date + datetime.timedelta(seconds=secs)
    print fulldate
    return fulldate

def createObj(dur, mins):
    date = datetime.datetime.now()
    trip = TripSuggestion(dur, addMins(date, mins))
    return trip
    
class TripOptimization:
    unknown = 0
    toLate = 1
    hurry = 2
    perfect = 3
    slowWalk = 4
    longTime = 5
    
class TripSuggestion:
    def __init__(self, duration, startDate, walkTime):
        self.duration = duration
        self.startDate = startDate
        self.level = TripOptimization.unknown
        self.walkTime = walkTime
        
    def getDepartureIn(self):
        td = self.startDate - datetime.datetime.now()
        return math.ceil(td.seconds / 60)
    
    def getLevel(self):
        suggestion = self.getDepartureIn()
        percentage = suggestion/self.walkTime

        if(percentage <= 0.70):
            return TripOptimization.toLate
    
        if(percentage > 0.70 and percentage <= 1.05):
            return TripOptimization.hurry
    
        if(percentage > 1.05 and suggestion <= self.walkTime + 3):
            return TripOptimization.perfect
            
        if(suggestion > self.walkTime + 3 and suggestion <= self.walkTime + 7):
            return TripOptimization.slowWalk

        return TripOptimization.longTime
        
    def getDisplayValue(self):
        return str(int(self.getDepartureIn()))

class Trip:
    def __init__(self, fromId, toId, walkTime):
        self.fromId = fromId
        self.toId = toId
        self.walkTime = walkTime
    
    
    
    def getSuggestions(self):
        p = Planner()
        suggestions = p.getDepartures(self.fromId, self.toId, self.walkTime)

            
        return suggestions

class Planner:
    def __init__(self):
        self.name = "planner"
        
    def getDepartures(self, fromId, toId, walktime):
        url = "http://api.sl.se/api2/TravelplannerV2/trip.json?key=d3904e7f7e9f40c7b3ef18d392dc19b7&originId=" + fromId + "&destId=" + toId + "&searchForArrival=0&realtime=true"
        response = json.load(urllib2.urlopen(url))
        trips = []
        
        for tripData in response['TripList']['Trip']:
            duration = int(tripData['dur'])
            leg = ''
            
            if(isinstance(tripData['LegList']['Leg'], list)):
                leg = tripData['LegList']['Leg'][0]
            else:
                leg = tripData['LegList']['Leg']
            
            dateString = ''
            
            if hasattr(leg['Origin'], 'rtDate'):
                dateString = leg['Origin']['rtDate'] + ' ' + leg['Origin']['rtTime']
            else:
                dateString = leg['Origin']['date'] + ' ' + leg['Origin']['time']

            date = datetime.datetime.strptime(dateString, '%Y-%m-%d %H:%M')
            trips.append(TripSuggestion(duration, date, walktime))
            
        return trips