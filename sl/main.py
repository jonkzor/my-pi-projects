import planner
#skarmarbrink = 9188
#hammarbyhojden = 9144
#centralen = 1002

p = planner.Trip("9188", "1002", 10)
suggestions = p.getSuggestions()

for suggestion in suggestions:
    print 'Duration: ' + str(suggestion.duration) + ' Departure: ' + suggestion.startDate.strftime("%H:%M") + ' Level: ' + str(suggestion.level)